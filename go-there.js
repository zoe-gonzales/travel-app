// Node modules
var dotenv = require('dotenv').config();
var country = require('countryjs');
var countryCode = require('iso-country-currency');
var weather = require('openweather-apis');
var Spotify = require('node-spotify-api');
var inquirer = require('inquirer');
var colors = require('colors');

// Setting up APIs
var keys = require('./keys.js');
weather.setAPPID(keys.openWeather);
var spotify = new Spotify(keys.spotify);

// Input Variables
var name = '';
var password = '';
var searchCountry = '';
var city = '';
var unit = '';
var genre = '';
var passwordTry = '';

// Helper Functions
// Capitalizes country name if input all lowercase
function capitalize(word) {
    if (word[0].toUpperCase() !== word[0]) {
        var char = word[0].toUpperCase();
        word = word.replace(word[0], char);
    }
    return word;
}

// Converts temps to F for US locations
function convertTemp(temp) {
    var x;
    x = Math.round((parseFloat(temp) * 1.8) + 32);
    return x;
}

// Converts speed to mph for US locations
function convertSpeed(speed) {
    var y;
    y = Math.round(parseFloat(speed) * 2.237);
    return y;
}

// Getting user input via inquirer
inquirer
.prompt([
    {
        type: 'input',
        message: 'Welcome! What is your name?',
        name: 'userName'
    },
    {
        type: 'password',
        message: 'Please choose a password',
        name: 'password'
    },
    {
        type: 'input',
        message: 'What country are you traveling to/within?',
        name: 'countryInput'
    },
    {
        type: 'input',
        message: 'What city are you traveling to?',
        name: 'cityInput'
    },
    {
        type: 'list',
        message: 'What is your preferred measurement system?',
        choices: ['Metric', 'US Standard'],
        name: 'unit'
    },
    {
        type: 'list',
        message: 'Please choose your favorite music genre:',
        choices: ['Pop', 'Hip-Hop', 'Mood', 'Workout', 'Decades', 'Country', 'Chill', 'Dance/Electronic', 'R&B', 'Rock', 'Indie', 'Party', 'Jazz', 'Afro', 'Gaming', 'Comedy', 'K-Pop', 'Soul', 'Punk'],
        name: 'genre'
    },
    {
        type: 'password',
        message: 'Please enter your password for results.',
        name: 'passwordTry'
    }
]).then(function(inquirerResponse) {
    // Saving inquirer input to variables
    name = inquirerResponse.userName;
    searchCountry = capitalize(inquirerResponse.countryInput);
    city = inquirerResponse.cityInput;
    unit = inquirerResponse.unit;
    password = inquirerResponse.password;
    genre = inquirerResponse.genre;
    passwordTry = inquirerResponse.passwordTry;
    // validating password
    if (passwordTry === password) {
        // This line converts the country name to ISO code
        var countryISO = countryCode.getISOByParam('countryName', searchCountry);
        // Gets info about country
        var response = country.info(countryISO);
        console.log(`\nHi ${name}! Here's the info you requested:`.cyan);
        // Displays in CL
        console.log(`\nKey details on ${response.name}:\n`.yellow);
        console.log(`Alternate spellings: ${response.altSpellings.join(', ')}`);
        console.log(`Capital: ${response.capital}`);
        console.log(`Demonym: ${response.demonym}`);
        console.log(`Approx. Population: ${response.population}`);
        console.log(`Continent: ${response.region}`);
        console.log(`Read more on Wikipedia: ${response.wiki}`);
        // Sets city for open weather
        weather.setCity(city);
        // Getting info from open weather
        weather.getAllWeather(function(err, obj){
            if (err) console.log(`Error: ${err}`); // checking for error
            console.log(`\nThe current weather in ${obj.name} is:\n`.yellow);
            if (unit === 'US Standard') {
                console.log(`Current Temperature: ${convertTemp(obj.main.temp)}° F`);
                console.log(`Low: ${convertTemp(obj.main.temp_min)}° F`);
                console.log(`High: ${convertTemp(obj.main.temp_max)}° F`);
                console.log(`Wind speed: ${convertSpeed(obj.wind.speed)}mph`);
            } else if (unit === 'Metric') {
                console.log(`Current Temp: ${obj.main.temp}° C`);
                console.log(`Low: ${obj.main.temp_min}° C`);
                console.log(`High: ${obj.main.temp_max}° C`);
                console.log(`Wind speed: ${obj.wind.speed}m/s`);
            } else {
                console.log(`Temperature could not be retrieved for this location.`);
            }
            console.log(`Humidity: ${obj.main.humidity}%`);
            console.log(`Condition: ${obj.weather[0].main}\n`);    
        });
        // Runs search for Spotify playlists based on genre input
        spotify.search({type: 'playlist', query: genre, limit: 10})
        .then(function(response) {
            console.log(`Your playlist selection is...\n`.yellow);
            var list = response.playlists.items;
            // Loops through first 10 playlists from that genre and displays in CL
            for (var i=0; i < list.length; i++) {
                var title = response.playlists.items[i].name;
                var link = response.playlists.items[i].external_urls.spotify
                console.log(title.cyan); 
                console.log(link);       
            }
        });
    } else {
        console.log(`Sorry, that was an incorrect password.`.red);
    }
});




