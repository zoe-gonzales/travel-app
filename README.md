# travel-app

### Overview
CLI app made in Node.js, designed to provide the user with basic country information, weather, and spotify playlist suggestions for their upcoming trip.

### Node modules used
dotenv, countryjs, iso-country-currency, openweather-apis, node-spotify-api, inquirer, colors

### APIs
Spotify, Open Weather Map

### Getting Started
* Open terminal or git bash and clone this repository in the folder of your choice
* Run ` npm install ` or ` npm i ` 
* A local .env file containing the Spotify id and secret and Open Weather Map key will need to be provided. Sign up for Spotify id and secret [here](https://developer.spotify.com/) and OWM [here](https://openweathermap.org/api).
* Run ` node go-there `
* Follow the prompts and re-enter your password correctly. Data will be returned.

**Note:** App does not support all commonly accepted names for countries. Ex: "Britain" will return an error; "Great Britain" will provide data.

### Demos

Correct password: https://drive.google.com/file/d/1IDEhbNtEb4V2alaJhRD0plShVZk-JPOH/view?usp=sharing

Incorrect password: https://drive.google.com/file/d/18IM5c6scTyqGoedqk4NH1XR5vPhccZiS/view?usp=sharing

