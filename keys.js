
var openWeather = process.env.APPID;

var spotify = {
    id: process.env.spotify_app_id,
    secret: process.env.spotify_app_secret
}

module.exports = {
    openWeather: openWeather,
    spotify: spotify
}